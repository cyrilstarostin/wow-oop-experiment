import creature.Creature;
import creature.Monster;
import creature.heroes.Archer;
import creature.heroes.Mage;
import creature.heroes.Warrior;
import creature.weapons.Axe;
import creature.weapons.Bow;
import creature.weapons.Quiver;
import creature.weapons.Staff;
import game.GameController;
import game.GameState;
import javafx.scene.shape.Arc;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        List<Creature> creatures = new ArrayList<>();
        creatures.add(new Archer("Артур", 99, 100, new Bow(30), new Quiver(50)));
        creatures.add(new Warrior("Женёк", 100, 50, new Axe(50, 1f)));
        creatures.add(new Mage("Василий", 40, 10, new Staff(50, 50)));
        creatures.add(new Monster("1", 15, 1, 10f));
        creatures.add(new Monster("2", 15, 1, 10f));
        creatures.add(new Monster("3", 15, 1, 10f));
        GameState state = new GameState(creatures);
        GameController controller = new GameController(state);
        controller.play();
    }
}
