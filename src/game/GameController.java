package game;

import creature.Creature;
import creature.Monster;
import creature.heroes.Hero;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class GameController {
    private GameState gameState;

    public GameController(GameState gameState) {
        this.gameState = gameState;
    }

    public void play() {
        for (int step = 0; !gameState.isFinished(); step++) {
            GameLogger.step(step);
            makeStep();
            removeDeadCreatures();
        }
        logWinner();
    }

    private void logWinner() {
        Creature winner = gameState.getWinner();
        if (winner == null) GameLogger.noWinner();
        else GameLogger.winner(winner);
    }

    private void makeStep() {
        for (Creature creature : gameState.getCreatures()) {
            if (!creature.isDead()) creature.makeStep(gameState);
        }
    }

    private void removeDeadCreatures() {
        List<Creature> deadCreatures = gameState.getDeadCreatures();
        gameState.removeCreatures(deadCreatures);
        GameLogger.killed(deadCreatures);
    }
}
