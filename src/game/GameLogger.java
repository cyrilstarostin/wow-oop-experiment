package game;

import creature.Creature;
import creature.Monster;
import creature.heroes.Archer;
import creature.heroes.Mage;
import creature.heroes.Warrior;

import java.util.List;

public final class GameLogger {

    public static void step(int step) {
        System.out.println("Шаг №" + step);
    }

    public static void killed(List<Creature> creatures) {
        for (Creature dead : creatures) {
            String classDead = getClass(dead) + ' ' + dead.getName();
            System.out.println(classDead + " погиб на поле брани");
        }
    }

    private static void killedHimself(Creature Creature) {
        String classCreature = getClass(Creature) + ' ' + Creature.getName();
        System.out.println(classCreature + " самоуничтожился");
    }

    public static void monsterAttacksTarget(Monster attacker, Creature target, float damage) {
        String classAttacker = getClass(attacker) + ' ' + attacker.getName();
        String classTarget = getClass(target) + ' ' + target.getName();
        System.out.println(classAttacker + " влетел с двух ног на игрока " + classTarget + ", и нанес " +
                damage + " урона");
    }

    public static void archerAttacksTarget(Archer attacker, Creature target, float damage) {
        String classAttacker = getClass(attacker) + ' ' + attacker.getName();
        String classTarget = getClass(target) + ' ' + target.getName();
        System.out.println(classAttacker + " выпустил стрелу в игрока " + classTarget + ", и нанес " + damage +
                " урона");
    }

    public static void warriorAttacksTarget(Warrior attacker, Creature target, float damage) {
        String classAttacker = getClass(attacker) + ' ' + attacker.getName();
        String classTarget = getClass(target) + ' ' + target.getName();
        System.out.println(classAttacker + " долбанул топором игрока " + classTarget + ", и нанес " + damage +
                " урона");
    }

    public static void wizardAttacksTarget(Mage attacker, Creature target, float damage) {
        String classAttacker = getClass(attacker) + ' ' + attacker.getName();
        String classTarget = getClass(target) + ' ' + target.getName();
        System.out.println(classAttacker + " кастует неведомые заклятия на игрока " + classTarget +
                ", и наносит " + damage + " урона");
    }

    public static void wizardAttacksButMisses(Mage attacker, Creature target) {
        String classAttacker = getClass(attacker) + ' ' + attacker.getName();
        String classTarget = getClass(target) + ' ' + target.getName();
        System.out.println(classAttacker + " кастует неведомые заклятия, но промазывает. " + classTarget + " цел");
    }

    public static void wizardAttacksHimself(Mage attacker, float damage) {
        String classAttacker = getClass(attacker) + ' ' + attacker.getName();
        System.out.println(classAttacker + " сражается на пределе, неудачнно кастует заклятие, и наносит себе " +
                damage + " урона");
    }

    static void winner(Creature Creature) {
        String classCreature = getClass(Creature) + ' ' + Creature.getName();
        System.out.println("Победитель этого сражения - " + classCreature);
    }

    static void noWinner() {
        System.out.println("У этой битвы, как и у любой другой, нет победителей");
    }

    public static void missesTurn(Creature Creature) {
        String classCreature = getClass(Creature) + ' ' + Creature.getName();
        System.out.println(classCreature + " пропускает ход - оружие не юзабельно");
    }

    public static void tookWeapon(Creature Creature) {
        String classCreature = getClass(Creature) + ' ' + Creature.getName();
        System.out.println(classCreature + " обнаружил новое оружие на поле боя, и снова боеспособен");
    }

    private static String getClass(Creature Creature) {
        if (Creature instanceof Archer) {
            return "Лучник";
        } else if (Creature instanceof Warrior) {
            return "Воин";
        } else if (Creature instanceof Mage) {
            return "Маг";
        } else {
            return "Монстр";
        }

    }
}

