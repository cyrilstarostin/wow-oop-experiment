package game;

import creature.Creature;
import creature.Monster;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class GameState {

    private List<Creature> creatures;
    private Random random = new Random();

    public GameState(List<Creature> creatures) {
        this.creatures = creatures;
        random.setSeed(System.currentTimeMillis());
    }

    public boolean isFinished() {
        return creatures.size() == 1 || creatures.isEmpty();
    }

    public boolean hasMonsters() {
        return getAliveMonsters().size() != 0;
    }

    public Monster findRandomAliveMonster() {
        if (!hasMonsters()) return null;

        List<Creature> monsters = getAliveMonsters();
        int randomIndex = random.nextInt(monsters.size());
        return (Monster) monsters.get(randomIndex);
    }

    public Creature findRandomAliveCreatureBut(Creature creature) {
        List<Creature> aliveCreatures = getAliveCreatures();
        if (aliveCreatures.isEmpty()) return null;

        aliveCreatures.remove(creature);
        int randomIndex = random.nextInt(aliveCreatures.size());
        return aliveCreatures.get(randomIndex);
    }

    private List<Creature> getAliveMonsters() {
        return creatures.stream()
                .filter(creature -> creature instanceof Monster && !creature.isDead())
                .collect(Collectors.toList());
    }

    public List<Creature> getAliveCreatures(){
        return creatures.stream()
                .filter(creature -> !creature.isDead())
                .collect(Collectors.toList());
    }

    public List<Creature> getCreatures() {
        return creatures;
    }

    public List<Creature> getDeadCreatures() {
        return creatures.stream().filter(Creature::isDead).collect(Collectors.toList());
    }

    public void removeCreatures(List<Creature> creatures) {
        this.creatures.removeAll(creatures);
    }

    public Creature getWinner() {
        if (creatures.isEmpty()) return null; else return creatures.get(0);
    }
}
