package creature;

public abstract class Creature implements Target, GameActor {

    protected String name;
    protected float health;
    protected int level;

    public Creature(String name, float health, int level) {
        this.name = name;
        this.health = health;
        this.level = level;
    }

    public boolean isDead() {
        return health <= 0;
    }

    public String getName() {
        return name;
    }
}
