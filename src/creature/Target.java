package creature;

public interface Target {

    void takeDamage(float damage);
}
