package creature;

import game.GameLogger;
import game.GameState;

public class Monster extends Creature {

    private float attackDamage;

    public Monster(String name, float health, int level, float attackDamage) {
        super(name, health, level);
        this.attackDamage = attackDamage;
    }

    @Override
    public void makeStep(GameState state) {
        Creature target = state.findRandomAliveCreatureBut(this);
        target.takeDamage(attackDamage);
        GameLogger.monsterAttacksTarget(this, target, attackDamage);
    }

    @Override
    public void takeDamage(float damage) {
        health -= damage;
    }
}
