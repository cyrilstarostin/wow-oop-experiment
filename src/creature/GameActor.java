package creature;

import game.GameState;

public interface GameActor {

    void makeStep(GameState state);
}
