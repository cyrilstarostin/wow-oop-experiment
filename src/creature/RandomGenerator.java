package creature;

public interface RandomGenerator {

    boolean isChance(Float probability);
}
