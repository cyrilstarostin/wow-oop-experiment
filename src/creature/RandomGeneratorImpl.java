package creature;

import java.util.Random;

public class RandomGeneratorImpl implements RandomGenerator {

    @Override
    public boolean isChance(Float probability) {
        return Math.random() >= probability;
    }
}
