package creature.weapons;

public class Bow extends Weapon {

    public Bow(float attackDamage) {
        super(attackDamage);
    }

    @Override
    public float getAttackDamage() {
        return attackDamage;
    }
}
