package creature.weapons;

public class Staff extends Weapon {
    private int magicLeft;

    public Staff(float attackDamage, int magicLeft) {
        super(attackDamage);
        this.magicLeft = magicLeft;
    }

    @Override
    public float getAttackDamage() {
        if (isTargetHit()) {
            return attackDamage;
        } else if (isSelfHit()) {
            return attackDamage;
        } else{
            return 0;
        }
    }

    private boolean isTargetHit() {
        float targetHitProbability = (100F - magicLeft) / 100;
        return Math.random() <= targetHitProbability;
    }

    public boolean isSelfHit() {
        return magicLeft < 10;
    }
}
