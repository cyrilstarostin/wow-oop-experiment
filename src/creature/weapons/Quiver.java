package creature.weapons;

public class Quiver {
    private int arrowCount;

    public Quiver(int arrowCount) {
        this.arrowCount = arrowCount;
    }

    public boolean hasArrows() {
        return arrowCount > 0;
    }

    public boolean takeNewArrow() {
        if (arrowCount > 0) {
            arrowCount--;
            return true;
        }
        return false;
    }
}