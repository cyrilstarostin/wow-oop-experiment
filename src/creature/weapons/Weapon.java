package creature.weapons;

public abstract class Weapon {
    protected float attackDamage;

    public Weapon(float attackDamage) {
        this.attackDamage = attackDamage;
    }

    public abstract float getAttackDamage();
}
