package creature.weapons;

public class Axe extends Weapon {
    private float bladeSharpness;

    public Axe(float attackDamage, float bladeSharpness) {
        super(attackDamage);
        this.bladeSharpness = bladeSharpness;
    }

    @Override
    public float getAttackDamage() {
        return attackDamage * bladeSharpness;
    }

    public void reduceSharpnessPerUsage(){
        bladeSharpness -= 0.05f;
        if (bladeSharpness <= 0.001f) bladeSharpness = 0f;
    }

    public boolean isBroken(){
        return bladeSharpness <= 0.001f;
    }
}
