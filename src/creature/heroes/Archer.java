package creature.heroes;

import creature.Creature;
import creature.Target;
import creature.weapons.Bow;
import creature.weapons.Quiver;
import game.GameLogger;

public class Archer extends Hero {

    private Quiver quiver;

    public Archer(String name, float health, int level, Bow bow, Quiver quiver) {
        super(name, health, level, bow);
        this.quiver = quiver;
    }

    @Override
    public void dealDamage(Creature target) {
        if (quiver.hasArrows()) {
            quiver.takeNewArrow();
            float damage = weapon.getAttackDamage();
            target.takeDamage(damage);
            GameLogger.archerAttacksTarget(this, target, damage);
        } else {
            GameLogger.missesTurn(this);
        }
    }
}