package creature.heroes;

import creature.Creature;
import creature.Target;
import creature.weapons.Staff;
import game.GameLogger;

public class Mage extends Hero {

    public Mage(String name, float health, int level, Staff staff) {
        super(name, health, level, staff);
    }

    @Override
    public void dealDamage(Creature target) {
        Staff staff = (Staff) weapon;
        float attackDamage = weapon.getAttackDamage();
        if (attackDamage == 0) {
            GameLogger.wizardAttacksButMisses(this, target);
            return;
        }
        if (staff.isSelfHit()) {
            takeDamage(attackDamage);
            GameLogger.wizardAttacksHimself(this, attackDamage);
        } else {
            target.takeDamage(attackDamage);
            GameLogger.wizardAttacksTarget(this, target, attackDamage);
        }
    }
}
