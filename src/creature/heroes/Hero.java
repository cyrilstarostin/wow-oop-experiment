package creature.heroes;

import creature.Creature;
import creature.weapons.Weapon;
import game.GameState;

public abstract class Hero extends Creature {

    protected KillStats killStats = new KillStats();
    protected Weapon weapon;

    public Hero(String name, float heath, int level) {
        super(name, heath, level);
    }

    public Hero(String name, float health, int level, Weapon weapon) {
        this(name, health, level);
        this.weapon = weapon;
    }

    protected abstract void dealDamage(Creature target);

    @Override
    public void takeDamage(float damage) {
        float totalDamage = damage + damage * killStats.calculatePurity();
        health -= totalDamage;
    }

    @Override
    public void makeStep(GameState gameState) {
        Creature target = findTarget(gameState);
        dealDamage(target);
    }

    private Creature findTarget(GameState gameState) {
        Creature target;
        if (gameState.hasMonsters()) {
            target = gameState.findRandomAliveMonster();
        } else {
            target = gameState.findRandomAliveCreatureBut(this);
        }
        return target;
    }

}
