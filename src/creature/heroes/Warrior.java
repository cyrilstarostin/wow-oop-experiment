package creature.heroes;

import creature.Creature;
import creature.Target;
import creature.weapons.Axe;
import game.GameLogger;

public class Warrior extends Hero {

    public Warrior(String name, float health, int level, Axe axe) {
        super(name, health, level, axe);
    }

    @Override
    public void dealDamage(Creature target) {
        Axe axe = (Axe) weapon;
        if (axe.isBroken()){
            GameLogger.missesTurn(this);
            return;
        }
        float damage = axe.getAttackDamage();
        target.takeDamage(damage);
        axe.reduceSharpnessPerUsage();
        GameLogger.warriorAttacksTarget(this, target, damage);
    }
}
