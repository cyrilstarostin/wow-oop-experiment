package creature.heroes;

public class KillStats {

    private int monstersKilled;
    private int heroesKilled;

    public KillStats(int monstersKilled, int heroesKilled) {
        this.monstersKilled = monstersKilled;
        this.heroesKilled = heroesKilled;
    }

    public KillStats() {
        this(0, 0);
    }

    public int calculatePurity() {
        if (heroesKilled == 0 && monstersKilled == 0) return 1;
        return (monstersKilled - heroesKilled) / (heroesKilled + monstersKilled);
    }

    public void addHeroesKilled(int count) {
        heroesKilled += count;
    }

    public void addMonstersKilled(int count) {
        monstersKilled += count;
    }
}
